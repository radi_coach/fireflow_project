# Setting Landing, Application, Admin Layouts
√ set layouts
√ set Landing layout
√ bootstrap layouts to routes
√ set Application layout
√ make some configurations
* set Admin and Profile Layout

# Game timer
√ module created
* stages

# Set FireBase
* init firebase
* set firebase auth
* set firestore

# Set Auth flow
* set login and signup pages
* set profile dashboard
* set personal info settings page

# Setting routes Meta Mixin
√ set index.html template
√ set meta tags
√ activate meta.js

# Team Management
* enterprise management tools/pages
* tbd - https://www.enterpriseready.io/features/teams/

# First Simulation
* Help Erik to finish setting variables and structure
* Help Erik to make logic
* Polish result
* Meanwhile, make simulation layout and components

# Simulation initiation and publications
* tbd
* leaderboards
* UX and workflow

# Product packages/tiers
* tbd

# Setting SSR
* investigate topic
* set local SSR
* deploy to FireBase and test

# Unit Testing
* make 1 reasonable test
* make 3 more reasonable tests
* make 6 more reasonable tests

# Onboarding components
* tbd

# Create Simulation Presets Builder
* make 2 types of presets
* make frontend for 2 presets
* make preset switcher
* make presets saving function

# Create Simulating setting API
* research topic
* make unstructured experiments
* think of a structure
* make an API
* implement
* rebuild simulations with an API

<!-- https://www.enterpriseready.io/ -->
# GDPR
## Based on - https://www.enterpriseready.io/gdpr/
* tbd

# Audit Logs
* tbd

# Role Based Access Control
* tbd

# Change Management Process
* determine processes
* determine product tiers based on process
* determine workflow toolkit
* tbd admin - send messages to user/group...

# Security
* determine security features
* unidentifiable data on main server / personal info separate - whitelist check

# Password settings
* tbd

# Information Security Policy, Incident Response Plan and Bug Bounty Program
* tbd

# Deployment automation
* determine approach for main and custom branches
* prepare scripts to auto deploy and error prevention

# Webhook notification system
* tbd
* internal and external use

# Integrations
* tbd - can be?

# Admin reports
* tbd
* user/group subscriptions

# SLA & WIKI
## Uptime - Always UP - Red, Blue, Green env.
* support - by tiers
* Documentation
* FAQ
* Tutorials and guides
* Release Notes

# Internationalization
* tbd

# A/B Testing
* target groups
* live changing
* "try this and say your oppinion" feature

# Fast Questions
* popup with question with simple options to answer

