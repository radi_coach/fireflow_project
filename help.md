# HELP

## SETTING GIT

* git init
* git remote add origin git@bitbucket.org:fireflowapp/fireflow.git
* git remote set-url origin git@bitbucket.org:fireflowapp/fireflow.git
* git add .
* git commit -m "Initial Commit"
* git push -u origin master
* git push -f origin master >> after reset to remove remote commits

## Some GIT Commands

* git status
* git log --oneline
* git checkout ComNUM/BrNAME
* git revert ComNUM
* git reset ComNUM
* git reset ComNUM --hard
* git branch BrNAME
* git checkout -b BrNAME
* git branch -D BrNAME
* git fetch --all --prune
* git fetch origin
* git reset --hard origin/master
* git push origin :BrandchName -> delete remote branch

## SYMBOLIC LINK

* in current folder create symbolic link to other directory
* ln -s ~/workspace/fireflow/src/store/modules/production/modules store