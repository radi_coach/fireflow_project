import FfApplicationLayout from './FfApplication'
import FfBlogLayout from './FfBlog'
import FfLandingLayout from './FfLanding'
import FfUserLayout from './FfUser'

export default {
  FfApplicationLayout,
  FfBlogLayout,
  FfLandingLayout,
  FfUserLayout
}
