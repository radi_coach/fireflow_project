// import store from '@/store'
import { auth } from '@/main'

export default (to, from, next) => {
  const user = auth.currentUser

  const path = to.path.match(/sign/g)
  if (path !== null && user) {
    next('/profile/')
  } else if (path !== null && !user) {
    next()
  } else {
    auth.onAuthStateChanged(u => {
      if (u) {
        next()
      } else {
        next('/signin')
      }
    })
  }
}
