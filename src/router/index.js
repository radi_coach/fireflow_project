import Vue from 'vue'
// import VueAnalytics from 'vue-analytics'
import Router from 'vue-router'
import Paths from './paths'

Vue.use(Router)

function route (obj) {
  return {
    path: `/${obj.path}`,
    meta: obj.meta,
    component: () => import(
      /* webpackChunkName: "routes" */
      /* webpackMode: "lazy-once" */
      `@/pages/${obj.view}/index.vue`
    ),
    beforeEnter: obj.beforeEnter || undefined
  }
}

const routes = Paths.map(r => route(r))

routes.push({ path: '*', redirect: '/' })

// Create a new router
const router = new Router({
  mode: 'history',
  routes
})

// Bootstrap Analytics
// Set in .env
// https://github.com/MatteoGabriele/vue-analytics
// if (process.env.GOOGLE_ANALYTICS) {
//   Vue.use(VueAnalytics, {
//     id: process.env.GOOGLE_ANALYTICS,
//     router,
//     autoTracking: {
//       page: process.env.NODE_ENV !== 'development'
//     }
//   })
// }

export default router
