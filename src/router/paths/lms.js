export default [
  {
    path: 'lms/controls',
    view: 'LMS/Controls',
    meta: {
      layout: 'user',
      title: 'LMS Controls',
      description: '',
      keywords: ''
    }
  },
  {
    path: 'lms/users',
    view: 'LMS/Users',
    meta: {
      layout: 'user',
      title: 'LMS Users',
      description: '',
      keywords: ''
    }
  },
  {
    path: 'lms/teams',
    view: 'LMS/Teams',
    meta: {
      layout: 'user',
      title: 'LMS Teams',
      description: '',
      keywords: ''
    }
  },
  {
    path: 'lms/statistics',
    view: 'LMS/Statistics',
    meta: {
      layout: 'user',
      title: 'LMS Statistics',
      description: '',
      keywords: ''
    }
  }
]
