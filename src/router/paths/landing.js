export default [
  {
    path: '',
    view: 'Landing/Home',
    meta: {
      layout: 'landing',
      title: 'Home',
      description: '',
      keywords: '',
      dark: true
    }
  },
  {
    path: 'about',
    view: 'Landing/About',
    meta: {
      layout: 'landing',
      title: 'About',
      description: 'FireFlow About',
      keywords: 'fireflow, about'
    }
  }
]
