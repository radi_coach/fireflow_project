import Guard from '@/router/guard'

export default [
  {
    path: 'ffadmin/posts',
    view: 'Admin/Posts',
    meta: {
      layout: 'user',
      title: 'Admin Posts',
      description: '',
      keywords: ''
    },
    beforeEnter: Guard
  },
  {
    path: 'ffadmin/posts/:id',
    view: 'Admin/Post',
    meta: {
      layout: 'user',
      title: 'Admin Post',
      description: '',
      keywords: ''
    },
    beforeEnter: Guard
  }
]
