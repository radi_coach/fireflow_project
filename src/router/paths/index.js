// import Guard from '@/router/guard'

import landing from './landing.js'
import admin from './admin.js'
import app from './app.js'
import auth from './auth.js'
import blog from './blog.js'
import lms from './lms.js'
import user from './user.js'

export default [
  ...landing,
  ...app,
  ...admin,
  ...auth,
  ...blog,
  ...lms,
  ...user,

  { path: '*',
    view: 'Page404',
    meta: {
      layout: 'landing',
      title: 'Page Not Found',
      description: '',
      keywords: ''
    }
  }
]
