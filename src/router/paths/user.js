import Guard from '@/router/guard'

export default [
  {
    path: 'profile',
    view: 'User/Profile',
    meta: {
      layout: 'user',
      title: 'Profile',
      description: '',
      keywords: ''
    },
    beforeEnter: Guard
  }
]
