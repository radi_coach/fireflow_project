import Guard from '@/router/guard'

export default [
  {
    path: 'signin',
    view: 'User/SignIn',
    meta: {
      layout: 'landing',
      title: 'SignIn',
      description: '',
      keywords: ''
    },
    beforeEnter: Guard
  },
  {
    path: 'signup',
    view: 'User/SignUp',
    meta: {
      layout: 'landing',
      title: 'SignUp',
      description: '',
      keywords: ''
    },
    beforeEnter: Guard
  }
]
