import Guard from '@/router/guard'

export default [
  {
    path: 'app',
    view: 'App',
    meta: {
      layout: 'application',
      title: 'App',
      description: '',
      keywords: ''
    },
    beforeEnter: Guard
  },
  {
    path: 'app/production',
    view: 'App/Production',
    meta: {
      layout: 'application',
      icon: 'home',
      title: 'Production',
      description: '',
      keywords: ''
    },
    beforeEnter: Guard
  }
]
