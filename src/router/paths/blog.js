export default [
  {
    path: 'blog',
    view: 'Blog',
    meta: {
      layout: 'blog',
      title: 'Blog',
      description: 'FireFlow Blog',
      keywords: 'fireflow, Blog',
      dark: true
    }
  },
  {
    path: 'blog/:id',
    view: 'Blog/Post',
    meta: {
      layout: 'blog',
      title: 'Post',
      description: 'FireFlow Post',
      keywords: 'fireflow, Post'
    }
  }
]
