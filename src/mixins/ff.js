import Store from '@/store'

export default {
  created () {
    Store.addModule('ff', {
      getters: {
        theme ({ state }) {
          return state.dark ? 'dark' : 'light'
        }
      },
      mutations: {
        toggle ({ state }) {
          state.dark = !state.dark
        }
      },
      state: {
        logo: 'dark',
        dark: false
      }
    })
  }
}
