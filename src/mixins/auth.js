import Vue from 'vue'
import Store from '@/store'
import { firestore, auth } from '@/main'

export default {
  created () {
    Store.addModule('auth', {
      mutations: {
        async autoSignIn ({ state }, payload) {
          const user = firestore.collection('users').doc(auth.currentUser.uid)

          state.user = payload
          const providerData = payload.providerData[0]

          const profileData = await user.get().then(doc => {
            if (doc.exists) return doc.data()
            else return {}
          })

          Vue.set(state, 'profile', {
            ...providerData,
            ...profileData
          })

          state.loading = false
        },

        signUserIn ({ state, commit }, payload) {
          state.loading = true
          commit.setError(null)
          auth.signInWithEmailAndPassword(payload.email, payload.password)
            .then(user => {
              const newUser = { id: user.uid }
              state.user = newUser
              state.profile = newUser.providerData[0]
              state.loading = false
            })
            .catch(error => {
              commit.setError(error)
              state.loading = false
            })
        },

        signUserUp ({ state, commit }, payload) {
          state.loading = true
          commit.setError(null)
          auth.createUserWithEmailAndPassword(payload.email, payload.password)
            .then(user => {
              const newUser = {
                id: user.uid
              }
              state.user = newUser
              state.profile = newUser.providerData[0]
              state.loading = false
            })
            .catch(error => {
              commit.setError(error)
              state.loading = false
            })
        },

        signOut ({ state }) {
          state.user = null
          auth.signOut()
        },

        setError ({ state }, payload) {
          state.error = payload
        },

        clearError ({ state }) {
          state.error = null
        },

        cancel ({ state }) {
          state.loading = false
        }
      },
      state: {
        user: null,
        profile: null,
        loading: true,
        error: false
      }
    })
  }
}
