import 'material-design-icons-iconfont'
import '@babel/polyfill'
import Vue from 'vue'
import '@/plugins/vuetify'
import i18n from '@/plugins/i18n'
import router from '@/router'
import store from '@/store'
import App from '@/App.vue'

import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

import Layouts from './layouts'
import Components from './components'
import './registerServiceWorker'

for (const key in Layouts) Vue.component(key, Layouts[key])
for (const key in Components) Vue.component(key, Components[key])

Vue.config.productionTip = false

const config = {
  apiKey: 'AIzaSyCHHOaJ5W4PbMtI2HlhSb-WrAk9McYJp10',
  authDomain: 'fireflow-pro.firebaseapp.com',
  databaseURL: 'https://fireflow-pro.firebaseio.com',
  projectId: 'fireflow-pro',
  storageBucket: 'fireflow-pro.appspot.com',
  messagingSenderId: '27606064265'
}
firebase.initializeApp(config)
export const auth = firebase.auth()
export const firestore = firebase.firestore()
firestore.settings({ timestampsInSnapshots: true })

// Change Locale Global Variable
Object.defineProperty(Vue.prototype, '$locale', {
  get: function () { return i18n.locale },
  set: function (locale) { i18n.locale = locale }
})

new Vue({
  router,
  store,
  i18n,
  created () {
    auth.onAuthStateChanged(user => {
      if (user) store.auth.autoSignIn(user)
      else store.auth.cancel()
    })
  },
  render: h => h(App)
}).$mount('#app')
