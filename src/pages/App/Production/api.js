// import Vue from 'vue'
import Store from '@/store'

import budget from './api/budget/module.js'
import materials from './api/materials/module.js'
import products from './api/products/module.js'
import lines from './api/lines/module.js'
import orders from './api/orders/module.js'
import managers from './api/managers/module.js'
import workTeams from './api/workTeams/module.js'
import suppliers from './api/suppliers/module.js'
import warehouseGoods from './api/warehouseGoods/module.js'
import warehouseMaterials from './api/warehouseMaterials/module.js'

import Timer from './api/timer/index.js'

export default function () {
  if (!Store.meta) {
    Store.addModule('meta', {
      mutations: { start ({ state }) { state.started = true } },
      state: { started: false }
    })
  }

  if (!Store.meta.state.started) {
    budget()
    materials()
    products()
    lines()
    orders()
    managers()
    workTeams()
    warehouseGoods()
    warehouseMaterials()
    suppliers()

    Store.addModule('timer', new Timer())

    // const timer = Store.timer
    // timer.start({ delay: 120 })

    // setTimeout(() => {
    //   timer.pause({
    //     delay: 20
    //   })
    // }, 1000 * 60)

    // const action = {
    //   name: 'Event 2',
    //   delay: 30,
    //   action () { console.log('Event 2 Action') },
    //   alert () { console.log('Alert: Started Event 2') },
    //   finished () { console.log('Alert: Finished Event 2') }
    // }

    // setTimeout(() => {
    //   timer.inject(action)
    // }, 1000 * 45)

    Store.meta.start()
  }
}
