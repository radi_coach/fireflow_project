import Store from '@/store'
import { orders as data } from '../values'

class Order {
  constructor (meta) {
    const products = Store.products

    this.name = meta.name || 'New Order'
    this.priority = meta.priority
    this.state = false

    this.items = meta.items.map(({ id, quantity }) => {
      return {
        id,
        quantity,
        name: products[id].name
      }
    })
  }

  // //////////////
  // PROTOTYPE AREA
  finish () {
    const budget = Store.budget
    const goods = Store.warehouseGoods

    const can = this.items.reduce((result, { id, quantity }) => {
      return result && goods.stock[id].quantity >= quantity
    }, true)

    if (!can) return

    this.items.forEach(({ id, quantity }) => {
      goods.ship({ id, quantity }, () => {
        budget.increase(goods.stock[id].price * quantity)
      })
    })
    this.state = true
  }
}

// ////////////
// STORE MODULE
const mod = 'orders'

class Orders {
  constructor () {
    this.state = data.map(meta => new Order(meta))
  }
}

export default function () { Store.addModule(mod, new Orders()) }
