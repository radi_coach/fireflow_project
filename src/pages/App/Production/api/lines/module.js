import Store from '@/store'
import { lines as data } from '../values'

class Line {
  constructor (meta) {
    this.name = meta.name || 'New Line'
    this.costToOperate = meta.costToOperate
    this.costOnOff = meta.costOnOff
    this.basicTechnicalLossesOfMaterials = meta.basicTechnicalLossesOfMaterials
    this.basicTechnicalLossesOfFinishGoods = meta.basicTechnicalLossesOfFinishGoods
    this.probOfBreaking = meta.probOfBreaking
    this.efficiencyOfProduction = meta.efficiencyOfProduction
    this.product = { ...meta.product }
    this.manager = undefined
    this.workTeam = undefined
    this.costOfMaterials = 0

    this.modifiers = {
      cost: 1,
      rate: 1
    }

    this.upgradeRestriction = meta.upgradeRestriction
    this.upgrades = {
      cost: 0.8,
      rate: 1.2,
      costOfUpgrade: 300
    }
  }

  // //////////////
  // PROTOTYPE AREA

  // canUpgrade () {
  //   return this.upgradeRestriction - 1 >= 0
  // },
  // upgrade () {
  //   if (this.canUpgrade()) {
  //     this.upgradeRestriction -= 1
  //     this.modifiers.cost *= this.upgrades.cost
  //     this.modifiers.rate *= this.upgrades.rate
  //     store.budget.current -= this.upgrades.costOfUpgrade
  //   }
  // }

  // resetModifiers () {
  //   for (const key in this.modifiers) this.modifiers[key] = 1
  // },

  // toggleTeam (team) {
  //   // this.resetModifiers()
  //   if (this.workTeam === team) {
  //     this.workTeam = undefined
  //   } else {
  //     this.workTeam = team
  //     this.modifiers.rate = team.efficiency
  //   }
  // }

  // toggleManager (manager) {
  //   this.resetModifiers()

  //   if (this.manager === manager) {
  //     this.manager = undefined
  //   } else {
  //     this.manager = manager

  //     for (const key in this.modifiers) {
  //       if (this.manager.lines[key]) {
  //         this.modifiers[key] = this.manager.lines[key]
  //       }
  //     }
  //   }
  // },

  produce () {
    const id = this.product.id
    const speed = this.product.basicSpeed
    const product = Store.products[id]
    const materials = Store.warehouseMaterials.materials
    const goods = Store.warehouseGoods
    const budget = Store.budget

    const can = product.materials.reduce((can, { id, rate }) => {
      return can && materials[id].quantity >= speed * rate
    }, true)

    if (!can) return
    budget.spend(
      this.costToOperate,
      () => {
        product.materials.forEach(({ id, rate }) => {
          materials[id].writeOff(speed * rate)
        })
        goods.store({ id, quantity: speed })
      }
    )
  }
}

// ////////////
// STORE MODULE
const mod = 'lines'

class Lines {
  constructor () {
    this.state = data.map(meta => new Line(meta))
  }
}

export default function () { Store.addModule(mod, new Lines()) }
