import Store from '@/store'
import { products as data } from '../values'

class Product {
  constructor (meta) {
    this.name = meta.name || 'New Product'
    this.quantity = meta.quantity
    this.productStage = meta.productStage
    this.palletization = meta.palletization
    this.costOfUtillize = meta.costOfUtillize
    this.materials = Store.bond(Store.materials, meta.materials)
    this.price = meta.price
  }
}

// ////////////
// STORE MODULE
const mod = 'products'

class Products {
  constructor () {
    this.state = data.map(meta => new Product(meta))
  }
}

export default function () { Store.addModule(mod, new Products()) }
