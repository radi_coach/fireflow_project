import Store from '@/store'
import { warehouseGoods as data } from '../values'

class Stock {
  constructor ({ id = 0, quantity = 0 }) {
    this.id = id
    this.quantity = quantity
  }

  get price () {
    return Store.products.state[this.id].price
  }

  get name () {
    return Store.products.state[this.id].name
  }
}

// ////////////
// STORE MODULE
const mod = 'warehouseGoods'

const getters = {
  total ({ state }) {
    return state.stock.reduce((total, product) => {
      return total + product.quantity
    }, 0)
  },

  names ({ state }) {
    return state.stock.map(item => {
      return Store.products.state[item.id].name
    })
  }
}

const mutations = {
  ship ({ state }, { id, quantity }, callback) {
    if (state.stock[id].quantity - quantity < 0) return
    state.stock[id].quantity -= quantity
    if (callback) callback()
  },

  store ({ state, get }, { id, quantity }) {
    if (get.total + quantity > state.capacity) return
    state.stock[id].quantity += quantity
  }
}

const state = {
  capacity: data.capacity,
  fixedCost: data.fixedCost,
  variableCost: data.variableCost,
  stock: data.stock.map(meta => new Stock(meta))
}

class WarehouseGoods {
  constructor () {
    this.getters = { ...getters }
    this.mutations = { ...mutations }
    this.state = { ...state }
  }
}

export default function () { Store.addModule(mod, new WarehouseGoods()) }
