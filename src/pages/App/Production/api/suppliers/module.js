import Store from '@/store'
import { suppliers as data } from '../values'

class Supplier {
  constructor (meta) {
    this.name = meta.name || 'New Supplier'
    this.id = meta.id
    this.minCapacity = meta.minCapacity || 0
    this.maxCapacity = meta.maxCapacity || 100

    this.materials = Store.bond(Store.materials, meta.materials)
  }

  buy (id, quantity) {
    const budget = Store.budget
    const whm = Store.warehouseMaterials

    const [material] = this.materials.filter(item => item.id === id)
    const costs = material.price * quantity

    budget.spend(costs, () => {
      whm.store({ id, sid: this.id, quantity })
    })
  }
}

// ////////////
// STORE MODULE
const mod = 'suppliers'

class Suppliers {
  constructor () {
    this.state = data.map(meta => new Supplier(meta))
  }
}

export default function () { Store.addModule(mod, new Suppliers()) }
