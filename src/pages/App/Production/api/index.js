export default {
  'ff-toolbar': () => import('./dom/FfToolbar.vue'),
  'ff-view-left': () => import('./dom/viewLeft.vue'),
  'ff-view-right': () => import('./dom/viewRight.vue'),
  'ff-products': () => import('./products/index.vue'),
  'ff-orders': () => import('./orders/index.vue'),
  'ff-workTeams': () => import('./workTeams/index.vue'),
  'ff-warehouseGoods': () => import('./warehouseGoods/index.vue'),
  'ff-warehouseMaterials': () => import('./warehouseMaterials/index.vue'),
  'ff-managers': () => import('./managers/index.vue')
}
