const budget = {
  current: 0,
  init: 10000
}

const orders = [
  {
    name: 'Order 1',
    priority: 1, // за выполнение приоритетного заказа экстраKPI,
    items: [
      {
        id: 0,
        quantity: 150
      },
      {
        id: 1,
        quantity: 100
      },
      {
        id: 2,
        quantity: 100
      }
    ]
  },
  {
    name: 'Order 2',
    priority: 0, //
    items: [
      {
        id: 0,
        quantity: 90
      },
      {
        id: 2,
        quantity: 150
      }
    ]
  }
]

const products = [
  {
    name: 'Product 1',
    productStage: 0, // может принимать целое значение [-1;1]
    palletization: 5, // сколько штук данного продукта помещается в одну паллету
    costOfUtillize: 1, //
    state: false,
    // quantity: 100,
    // sold: 0,
    // cost: 40,
    price: 150,
    materials: [
      {
        id: 0,
        rate: 5
      },
      {
        id: 1,
        rate: 2
      }
    ]
  },
  {
    name: 'Product 2',
    productStage: 0, // может принимать целое значение [-1;1]
    palletization: 5, // сколько штук данного продукта помещается в одну паллету
    costOfUtillize: 1, //
    state: false,
    // quantity: 300,
    // sold: 0,
    // cost: 30,
    price: 100,
    materials: [
      {
        id: 0,
        rate: 2
      },
      {
        id: 2,
        rate: 10
      }
    ]
  },
  {
    name: 'Product 3',
    productStage: 0, // может принимать целое значение [-1;1]
    palletization: 5, // сколько штук данного продукта помещается в одну паллету
    costOfUtillize: 1, //
    state: false,
    // quantity: 500,
    // sold: 0,
    // cost: 20,
    price: 40,
    materials: [
      {
        id: 1,
        rate: 3
      },
      {
        id: 2,
        rate: 4
      }
    ]
  }
]

const suppliers = [
  {
    name: 'Supplier 1',
    id: 0,
    minCapacity: 100,
    maxCapacity: 600,
    materials: [
      {
        id: 0,
        quality: 0.8,
        price: 8
      },
      {
        id: 2,
        quality: 0.9,
        price: 10
      }
    ]
  },
  {
    name: 'Supplier 2',
    id: 1,
    minCapacity: 200,
    maxCapacity: 700,
    materials: [
      {
        id: 0,
        quality: 0.95,
        price: 12
      },
      {
        id: 1,
        quality: 0.95,
        price: 6
      },
      {
        id: 2,
        quality: 0.85,
        price: 8
      }
    ]
  }
]

const materials = [
  {
    name: 'Material 1',
    type: 'pack', // материал типа pack используется только 1 на единицу продукта
    costUtillize: 0.4, //
    palletization: 5, //
    // quantity: 0,
    // init: 100,
    cost: 10
    // pack: 10
  },
  {
    name: 'Material 2',
    type: 'raw', //
    costUtillize: 0.4, //
    palletization: 5, //
    // quantity: 0,
    // init: 700,
    cost: 20
    // pack: 15
  },
  {
    name: 'Material 3',
    type: 'pack', // материал типа pack используется только 1 на единицу продукта
    costUtillize: 0.4, //
    palletization: 5, //
    // quantity: 0,
    // init: 1300,
    cost: 5
    // pack: 20
  }
]

const lines = [ // lines ссылается на другую библиотеку с линиями
  {
    name: 'Line 1',
    modifiers: 1, //
    costOnOff: 8, //
    basicTechnicalLossesOfMaterials: 0.02, //
    basicTechnicalLossesOfFinishGoods: 0.005, //
    probOfBreaking: 0.2, //
    // repair length (обдумать)
    efficiencyOfProduction: 0.9, //
    costToOperate: 150, // costToOperate
    upgradeRestriction: 8,
    product: {
      id: 0,
      basicSpeed: 5
    }
  },
  {
    name: 'Line 2',
    modifiers: 1, //
    costOnOff: 8,
    basicTechnicalLossesOfMaterials: 0.02, //
    basicTechnicalLossesOfFinishGoods: 0.005, //
    probOfBreaking: 0.2, //
    // repair length (обдумать)
    efficiencyOfProduction: 0.9, //
    costToOperate: 150, // costToOperate
    upgradeRestriction: 8,
    product: {
      id: 1,
      basicSpeed: 6
    }
  },
  {
    name: 'Line 3',
    modifiers: 1, //
    costOnOff: 8,
    basicTechnicalLossesOfMaterials: 0.02, //
    basicTechnicalLossesOfFinishGoods: 0.005, //
    probOfBreaking: 0.2, //
    // repair length (обдумать)
    efficiencyOfProduction: 0.9, //
    costToOperate: 150, // costToOperate
    upgradeRestriction: 8,
    product: {
      id: 2,
      basicSpeed: 6
    }
  }
]

const managers = [
  {
    name: 'Anton',
    shiftsPerPeriod: 12, //
    hoursPerShift: 8, //
    costPerShift: 300, //
    costPerHourOvertime: 75, //
    abilities: { //
      process: 0.6, // x
      projects: 0.4, // 1-x
      overtime: 0.6
    },
    happiness: 1,
    learner: 0.4, // x
    teacher: 0.6, // 1-x
    skills: {
      lines: 0.6,
      workTeams: 0.4,
      warehouse: 0.5,
      materials: 0.4
    }
  },
  {
    name: 'George',
    shiftsPerPeriod: 12, //
    hoursPerShift: 8, //
    costPerShift: 300, //
    costPerHourOvertime: 75, //
    abilities: { //
      process: 0.6, // x
      projects: 0.4, // 1-x
      overtime: 0.6
    },
    happiness: 1,
    learner: 0.4, // x
    teacher: 0.6, // 1-x
    skills: {
      lines: 0.6,
      workTeams: 0.4,
      warehouse: 0.5,
      materials: 0.4
    }
  },
  {
    name: 'Ivan',
    shiftsPerPeriod: 12, //
    hoursPerShift: 8, //
    costPerShift: 300, //
    costPerHourOvertime: 75, //
    abilities: { //
      process: 0.6, // x
      projects: 0.4, // 1-x
      overtime: 0.6
    },
    happiness: 1,
    learner: 0.4, // x
    teacher: 0.6, // 1-x
    skills: {
      lines: 0.6,
      workTeams: 0.4,
      warehouse: 0.5,
      materials: 0.4
    }
  }
]

const workTeams = [
  {
    name: 'workTeam 1',
    shiftsPerPeriod: 10, //
    hoursPerShift: 8, //
    costPerShift: 100, //
    costPerHourOvertime: 25, //
    overtimeCapacity: 30, //  возможное кол-во часов переработки за период
    efficiency: {
      line1: 0.9,
      line2: 0.85,
      line3: 0.7
    }
  },
  {
    name: 'workTeam 2',
    shiftsPerPeriod: 10, //
    hoursPerShift: 8, //
    costPerShift: 100, //
    costPerHourOvertime: 25, //
    overtimeCapacity: 30, //  возможное кол-во часов переработки за период
    efficiency: {
      line1: 0.9,
      line2: 0.7,
      line3: 0.8
    }
  },
  {
    name: 'workTeam 3',
    shiftsPerPeriod: 10, //
    hoursPerShift: 8, //
    costPerShift: 100, //
    costPerHourOvertime: 25, //
    overtimeCapacity: 30, //  возможное кол-во часов переработки за период
    efficiency: {
      line1: 0.8,
      line2: 0.9,
      line3: 0.7
    }
  }
]

const warehouseGoods = {
  capacity: 500, // pallets
  stock: [
    {
      id: 0,
      quantity: 300
    },
    {
      id: 1,
      quantity: 15
    },
    {
      id: 2,
      quantity: 170
    }
  ],
  fixedCost: 100, // привязка ко времени
  variableCost: 5 // привязка ко времени
}

const warehouseMaterials = {
  capacity: 500,
  materials: [
    { id: 0,
      total: 0,
      sid: 1,
      source: [
        { id: 1, quantity: 95 },
        { id: 0, quantity: 50 }
      ]
    },
    { id: 1,
      total: 0,
      sid: 1,
      source: [
        { id: 1, quantity: 60 }
      ]
    },
    { id: 2,
      total: 0,
      sid: 0,
      source: [
        { id: 1, quantity: 70 },
        { id: 0, quantity: 80 }
      ]
    }
  ],
  fixedCost: 100, // привязка ко времени
  variableCost: 5 // привязка ко времени
}

const repairBrigade = {
  name: 'repair Brigade',
  shiftsPerPeriod: 10, //
  hoursPerShift: 8, //
  costPerShift: 100, //
  costPerHourOvertime: 25, //
  overtimeCapacity: 1, //  возможное кол-во часов переработки за период
  efficiency: {
    line1: 0.5,
    line2: 0.5,
    line3: 0.5
  }
}
// upgrades
// cost

// type
// are of production
// hours required
// aditional budget (only on type: project)

export {
  budget,
  orders,
  products,
  materials,
  lines,
  managers,
  workTeams,
  warehouseGoods,
  warehouseMaterials,
  suppliers,
  repairBrigade
}
