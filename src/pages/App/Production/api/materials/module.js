import Store from '@/store'
import { materials as data } from '../values'

class Material {
  constructor (meta) {
    this.name = meta.name || 'New Material'
    this.type = meta.type
  }
}

// ////////////
// STORE MODULE
const mod = 'materials'

class Materials {
  constructor () {
    this.state = data.map(meta => new Material(meta))
  }
}

export default function () { Store.addModule(mod, new Materials()) }
