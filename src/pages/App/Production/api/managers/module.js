import Store from '@/store'
import { managers as data } from '../values'

class Manager {
  constructor (meta) {
    this.name = meta.name || 'New Manager'
    this.shiftsPerPeriod = meta.shiftsPerPeriod
    this.hoursPerShift = meta.hoursPerShift
    this.costPerShift = meta.costPerShift
    this.costPerHourOvertime = meta.costPerHourOvertime
    this.overtimeCapacity = meta.overtimeCapacity
    this.abilities = { ...meta.abilities }
    this.skills = { ...meta.skills }
    this.happiness = meta.happiness
    this.learner = meta.learner
    this.teacher = meta.teacher
    this.id = meta.id
    this.lines = { ...meta.lines }
  }
}

// ////////////
// STORE MODULE
const mod = 'managers'

class Managers {
  constructor () {
    this.getters = {
      names () {
        return this.map(item => item.name)
      }
    }

    this.mutations = {
      add ({ state }, payload) {
        state.push(payload)
      }
    }

    this.state = data.map(meta => new Manager(meta))
  }
}

export default function () { Store.addModule(mod, new Managers()) }
