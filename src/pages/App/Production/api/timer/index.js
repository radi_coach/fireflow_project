let worker = null

const mutations = {
  inject ({ state }, event) {
    state.events.push({
      ...event,
      trigger: state.time + event.delay
    })

    if (event.alert) event.alert.apply()
  },

  pause ({ state }, { delay }) {
    worker.postMessage({ hold: delay })
  },

  stop ({ state }) {
    if (worker) {
      worker.terminate()
      worker = undefined
      state.state = 'off'
    }
  },

  start ({ state, commit }, { delay, options }) {
    state.state = 'on'
    state.time = 0

    worker.postMessage({ delay: delay, state: state.state })

    worker.onmessage = (event) => {
      const { time, type, hold } = event.data

      if (type === 'time') {
        state.time = time
        const events = state.events.filter(event => state.time >= event.trigger)

        events.forEach(event => {
          event.action.apply()
          if (event.finished) event.finished.apply()
          state.events.splice(events.indexOf(event), 1)
        })
      }

      if (type === 'hold') state.hold = hold

      if (type === 'stop') commit.stop()
    }
  }
}

export default class Timer {
  constructor (callback) {
    this.state = {
      time: null,
      state: 'off',
      events: [],
      hold: 0
    }

    this.mutations = mutations

    if (typeof (Worker) !== 'undefined') {
      if (worker !== null) return
      worker = createWorker(timeWorker)
    }

    if (callback) callback()
  }
}

function timeWorker () {
  let timerState = 'off'
  let time = 0
  let delay = null
  let hold = 0

  const timer = setInterval(() => {
    if (hold > 0) {
      timerState = 'paused'
      hold--
      postMessage({ type: 'hold', hold })
    } else {
      timerState = 'on'
    }

    if (timerState !== 'on') return
    time++
    postMessage({ type: 'time', time })

    if (time >= delay) {
      postMessage({ type: 'stop', time })
      clearInterval(timer)
    }
  }, 1000)

  this.onmessage = (e) => {
    timerState = timerState || e.data.state
    delay = delay || e.data.delay
    hold = e.data.hold || 0
  }
}

function createWorker (fn) {
  return new Worker(URL.createObjectURL(new Blob(['(' + fn + ')()'])))
}
