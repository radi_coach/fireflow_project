import Store from '@/store'
import { budget as data } from '../values'

// ////////////
// STORE MODULE
const mod = 'budget'

const mutations = {
  increase ({ state }, payload) {
    state.current += payload
    return payload
  },
  /**
   * Decrease Current Budget for some Value and Make action after
   * @param {number} payload - Amount witch will be withdrawn from budget.current.
   * @param {function} callback - Define a function that will do something as a result of spending budget.
   */
  spend ({ state }, payload, callback) {
    if (state.current - payload < 0) return
    else state.current -= payload

    if (callback) callback()
  }
}

class Budget {
  constructor () {
    this.mutations = mutations
    this.state = { current: data.init }
  }
}

export default function () { Store.addModule(mod, new Budget()) }
