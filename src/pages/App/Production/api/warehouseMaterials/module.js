import Store from '@/store'
import { warehouseMaterials as data } from '../values'

// ////
// ITEM

class Stock {
  constructor ({ id = 0, total = 0, source = [], sid = 0 }) {
    this.id = id
    this.total = total
    this.source = [...source]

    this.sid = sid
  }

  get name () {
    return Store.materials[this.id].name
  }

  get quantity () {
    return this.source.reduce((sum, item) => {
      return sum + item.quantity
    }, 0)
  }

  sell (quantity) {
    if (this.quantity - quantity < 0) return
    const [material] = Store.suppliers[this.sid].materials.filter(item => item.id === this.id)
    Store.budget.increase(quantity * material.price * 0.5)
    this.writeOff(quantity)
  }

  writeOff (quantity) {
    if (this.quantity - quantity < 0) return

    const diff = this.source.map(item => {
      let delta
      if (item.quantity - quantity > 0) {
        delta = item.quantity - quantity
        quantity = 0
      } else {
        delta = 0
        quantity -= item.quantity
      }

      return delta
    })

    this.source.forEach((item, index) => {
      item.quantity = diff[index]
    })
  }
}

// ////////////
// STORE MODULE
const mod = 'warehouseMaterials'

const getters = {
  total ({ state }) {
    return state.materials.reduce((total, material) => {
      material.total = material.source.reduce((sum, stack) => sum + stack.quantity, 0)
      return total + material.total
    }, 0)
  }
}

const mutations = {
  store ({ state, get }, { id, sid, quantity }) {
    if (get.total + quantity > state.capacity) return

    const [source] = state.materials[id].source
      .filter(item => item.id === sid)

    source.quantity += quantity
  }
}

const state = {
  capacity: data.capacity,
  fixedCost: data.fixedCost,
  variableCost: data.variableCost,
  materials: data.materials.map(meta => new Stock(meta))
}

class WarehouseMaterials {
  constructor () {
    this.getters = { ...getters }
    this.mutations = { ...mutations }
    this.state = { ...state }
  }
}

export default function () { Store.addModule(mod, new WarehouseMaterials()) }
