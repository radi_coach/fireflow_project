import Store from '@/store'
import { workTeams as data } from '../values'

class WorkTeam {
  constructor (meta) {
    this.name = meta.name || 'New workTeam'
    this.shiftsPerPeriod = meta.shiftsPerPeriod
    this.hoursPerShift = meta.hoursPerShift
    this.costPerShift = meta.costPerShift
    this.costPerHourOvertime = meta.costPerHourOvertime
    this.overtimeCapacity = meta.overtimeCapacity
    this.efficiency = { ...meta.efficiency }
  }
}

// ////////////
// STORE MODULE
const mod = 'workTeams'

const getters = {
  names () {
    return this.map(item => item.name)
  }
}

class WorkTeams {
  constructor () {
    this.getters = { ...getters }
    this.state = data.map(meta => new WorkTeam(meta))
  }
}

export default function () { Store.addModule(mod, new WorkTeams()) }
