class Map {
  constructor () {
    this.svg = undefined
    this.point = undefined
    this.viewBox = undefined

    this.x = 0
    this.y = 0

    this.isDragging = false

    this.isPointerDown = false
    this.pointerOrigin = undefined

    this.objects = {
      luxary: {
        path: 'M0 84h183v171H0z',
        dialog: false,
        event: () => {
          this.objects.luxary.dialog = true
        }
      },
      market: {
        path: 'M389 25c0-14-11-25-25-25H258c-14 0-25 11-25 25v51c0 13 11 25 25 25h106c14 0 25-12 25-25V25z',
        dialog: false,
        event: () => {
          this.objects.luxary.market = true
        }
      },
      blacksmith: {
        path: 'M370 214l95 162H275l95-162z',
        dialog: false,
        event: () => {
          this.objects.luxary.blacksmith = true
        }
      }
    }
  }

  init (ref) {
    this.svg = ref

    // @ts-ignore
    if (window.PointerEvent) {
      this.svg.addEventListener('pointerdown', this.onPointerDown)
      this.svg.addEventListener('pointerup', this.onPointerUp)
      this.svg.addEventListener('pointerleave', this.onPointerUp)
      this.svg.addEventListener('pointermove', this.onPointerMove)
    } else {
      this.svg.addEventListener('mousedown', this.onPointerDown)
      this.svg.addEventListener('mouseup', this.onPointerUp)
      this.svg.addEventListener('mouseleave', this.onPointerUp)
      this.svg.addEventListener('mousemove', this.onPointerMove)

      this.svg.addEventListener('touchstart', this.onPointerDown)
      this.svg.addEventListener('touchend', this.onPointerUp)
      this.svg.addEventListener('touchmove', this.onPointerMove)
    }

    this.point = this.svg.createSVGPoint()
    this.viewBox = this.svg.viewBox.baseVal
  }

  getPointFromEvent (event) {
    if (event.targetTouches) {
      this.point.x = event.targetTouches[0].clientX
      this.point.y = event.targetTouches[0].clientY
    } else {
      this.point.x = event.clientX
      this.point.y = event.clientY
    }

    const invertedSVGMatrix = this.svg.getScreenCTM().inverse()

    return this.point.matrixTransform(invertedSVGMatrix)
  }

  onPointerDown = (event) => {
    event.preventDefault()
    this.isPointerDown = true
    this.pointerOrigin = this.getPointFromEvent(event)
  }

  onPointerUp = (event) => {
    const key = event.target.id
    console.log(key)
    if (this.objects[key]) {
      if (!this.isDragging) this.objects[key].event()
    }
    this.isPointerDown = false
    this.isDragging = false
  }

  onPointerMove = (event) => {
    if (!this.isPointerDown) return
    event.preventDefault()

    const pointerPosition = this.getPointFromEvent(event)
    const deltaX = pointerPosition.x - this.pointerOrigin.x
    const deltaY = pointerPosition.y - this.pointerOrigin.y

    this.viewBox.x -= deltaX
    this.viewBox.y -= deltaY

    const deltaStep = 0.4
    if (
      deltaX > deltaStep ||
      deltaX < -deltaStep ||
      deltaY > deltaStep ||
      deltaY < -deltaStep
    ) this.isDragging = true

    this.x = this.viewBox.x
    this.y = this.viewBox.y
  }
}

export default new Map()
