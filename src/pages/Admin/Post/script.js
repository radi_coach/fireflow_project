import { firestore } from '@/main'
import Quill from 'quill'

const blog = firestore.collection('blog')

export default {
  data () {
    return {
      saving: false,
      loading: false,
      post: null,
      tags: ['Guest Post', 'HR', 'EdTech'],
      editor: null
    }
  },

  mounted () {
    const postRef = blog.doc(this.$route.params.id)

    postRef.get()
      .then(post => {
        this.post = { id: post.id, ...post.data() }
      }).then(() => {
        this.setEditor(this.post.content)
      })
  },

  methods: {
    savePost (post, action = 'none') {
      this.saving = true
      const p = JSON.parse(JSON.stringify(post))
      const postRef = blog.doc(p.id)
      const date = new Date()

      p.content = this.editor.getContents().ops
      p.updated = date

      delete p.created
      delete p.id

      if (action === 'publish') {
        p.published = date
      } else if (action === 'draft') {
        p.published = null
      }

      postRef.update({
        ...p
      }).then(() => {
        this.saving = false
      })

      // Renderer update
      this.post.updated.seconds = date.getTime() / 1000
      if (action === 'publish') {
        this.post.published = {
          seconds: date.getTime() / 1000
        }
      }
    },

    publishPost (post) {
      if (!confirm('Are you ready to Publish this post?!')) return

      post.state = 'published'
      this.savePost(post, 'publish')
    },

    unPublishPost (post) {
      if (!confirm('Sure you want to Unpublish this post?!')) return

      post.state = 'draft'
      this.savePost(post, 'draft')
    },

    deletePost (id) {
      if (!confirm('Sure you want to Delete this post?!')) return

      blog.doc(id).delete().then(() => {
        this.post = null
        this.$router.go(-1)
      })
    },

    setEditor (content) {
      const toolbar = [
        ['bold', 'italic', 'underline', 'strike'],
        ['blockquote', 'code-block'],
        [{ 'header': 1 }, { 'header': 2 }],
        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
        ['image']
      ]

      const options = {
        modules: {
          toolbar
        },
        placeholder: 'Compose an epic Article...',
        theme: 'snow'
      }

      this.editor = new Quill(this.$refs.editor, options)
      this.editor.setContents(content)
    },

    removeTag (item) {
      this.post.chips.splice(this.post.chips.indexOf(item), 1)
      this.post.chips = [...this.post.chips]
    }
  }
}
