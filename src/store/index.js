import Vue from 'vue'

// Store
//  | module
//    | state
//    | get
//    | commit
//    | dispatch
//    | proxy

/**
* Add Module to Store.
* @param {string} key - Modules Key, must be unique
* @param {object} mod - Module with optional { actions, getters, mutations, state }
*/
function addModule (key, mod) {
  if (this[key]) return

  const newMod = {
    module: key,
    dispatch: {},
    get: {},
    commit: {},
    state: mod.state
  }

  for (const key in mod.getters) {
    Object.defineProperty(newMod.get, key, {
      get: () => { return mod.getters[key].call(mod.state, { state: newMod.state }) }
    })
  }

  for (const key in mod.mutations) {
    newMod.commit[key] = function (payload, callback) {
      mod.mutations[key].call(
        newMod.state, { state: newMod.state, commit: newMod.commit, get: newMod.get }, payload, callback
      )
    }
  }

  newMod.proxy = new Proxy(newMod, {
    get (target, prop) {
      if (prop in target.state) return target.state[prop]
      else if (prop in target.get) return target.get[prop]
      else if (prop in target.commit) return target.commit[prop]
      else if (prop in target.dispatch) return target.dispatch[prop]
      else return target.state
    }
  })

  Vue.set(this, key, newMod)
}

function bond (storeTarget, props) {
  if (props instanceof Array) {
    return props.map(item => {
      const id = item.id
      return new Proxy(item, {
        get (target, prop) {
          if (prop in target) return target[prop]
          else if (prop in storeTarget[id]) return storeTarget[id][prop]
          return undefined
        }
      })
    })
  } else return undefined
}

class Store {
  constructor (modz = {}) {
    for (const k in modz) this.addModule(k, modz[k])
    this.init()
  }
  init = init
  addModule = addModule
  removeModule (key) { delete this[key] }
  bond = bond
}

function init () {
  this._vm = new Vue({
    data: { state: this }
  })
}

Store.install = function () {
  Vue.prototype.$store = store
}

const store = new Proxy(new Store(), {
  get (target, prop) {
    if (target[prop]) {
      if (target[prop].module) return target[prop].proxy
      else return target[prop]
    } else {
      return undefined
    }
  }
})

Vue.use(Store)

export default store
